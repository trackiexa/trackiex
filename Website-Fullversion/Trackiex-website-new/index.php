
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="">
	<meta name="author" content="">

	<title>TrackieX - Home</title>
	<!-- Standard Favicon -->
	<link rel="icon" href="img/fav/android-icon-96x96.png">
	<link rel="icon" href="img/fav/favicon.ico">

	<!-- Touch Icons - iOS and Android 2.1+ -->
	<link rel="apple-touch-icon" href="img/fav/android-icon-48x48.png">
	<link rel="apple-touch-icon" sizes="72x72" href="img/fav/apple-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="img/fav/android-icon-144x144.png" />

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
	<link rel="stylesheet" type="text/css" href="css/swiper.min.css">
	<link rel="stylesheet" type="text/css" href="css/hover-min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.theme.default.min.css">

	<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel="stylesheet">
	<script src="js/vendor/modernizr.js"></script>

	<!--[if lt IE 9]>
	<script src="js/html5/respond.min.js"></script>
	<![endif]-->

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '242376376362098');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=242376376362098&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            facebook: "304390876779767", // Facebook page ID
            call_to_action: "Message us", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="60">
<div class="se-pre-con"></div>

<!-- main nav start -->

<nav class="navbar navbar-expand-lg fixed-top center-brand static-nav">
	<!--<div id="menu_bars_initial" class="right menu_bars_initial">-->
		<!--<div class="btn-group" id="extra_nav_">-->
			<!--<a href="#portfolio" class="btn">Order Now</a>-->
		<!--</div>-->
	<!--</div>-->
	<div class="container">
		<a class="navbar-brand" href="#">
			<img src="img/logo-colored-main.png" alt="logo" class="logo-default">
		</a>
		<button class="navbar-toggler navbar-toggler-right collapsed" type="button" data-toggle="collapse" data-target="#xenav">
			<i class="fas fa-bars fa-2x"></i>
		</button>
		<div class="collapse navbar-collapse" id="xenav">
			<ul class="navbar-nav" id="container">
				<li class="nav-item">
					<a class="nav-link active" href="https://trackiex.com/Trackiex-blog/how-it-works/">How it Works </a>
				</li>
				<!--<li class="nav-item">
					<a class="nav-link" href="#">Blog </a>
				</li>-->
				<li class="nav-item">
					<a class="nav-link" href="https://trackiex.com/Trackiex-blog/faq/">FAQ </a>
				</li>


			</ul>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="#story">About Us</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" href="https://trackiex.com/Trackiex-blog/contact-us/">Contact Us</a>
				</li>

                <li class="nav-item hide-desk-only">
                    <a class="nav-link" href="https://trackiex.com/Trackiex-blog/product/trackiex-kids-2g/">Pre-Order Now</a>
                </li>



			</ul>
		</div>

	</div>
	<div id="menu_bars_" class="right menu_bars hidden-mobile hide-tab">
		<div class="btn-group" id="extra_nav_one">
			<a href="https://trackiex.com/Trackiex-blog/product/trackiex-kids-2g/" class="btn">Pre-Order Now</a>
		</div>
	</div>
	<!--/.CONTAINER-->

</nav>
<!-- /.navbar -->

<!--BANNER-->
<header class="top_banner desktop-version" id="home">
	<div class="container_custom">
		<div class="row h-100">
				<div class="col-md-6 align-self-center wow animate fadeInLeft">
					<div class="about_left_txt">
						<div class="title  title-header-cus-width">
							<p class="title-banner-trackie">Be There For Your Children, Even When You're Apart</p>

						</div><!--/.title-->

						<div class="sub_txt  title-header-cus-width-description">
							<p class="title-banner-trackie-description">With the TrackieX GPS tracker and mobile app, you’ll be able to know where your children are in real-time so you can have peace of mind wherever they go.
							</p>


						</div>

						<div id="menu_bars" class="left menu_bars_head_btn">
							<div class="btn-group" id="extra_nav">
								<a href="https://trackiex.com/Trackiex-blog/product/trackiex-kids-2g/" class="btn">Pre-Order Now</a>
							</div>
						</div>
					</div><!--/.about_left_txt-->
				</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 align-self-end display-table mob-margin sm-margin-50px-bottom xs-margin-30px-bottom wow animate fadeInRight" >
				<div class="display-table-cell vertical-align-middle">
					<a class="popup-youtube" href="https://www.youtube.com/watch?v=smZsZ7xafQQ">
						<img src="img/header-mob-phone.png"  alt="" class="width-100 img-responsive" data-no-retina="">
						<div class="icon-play">
							<div class="absolute-middle-center">
								<img src="img/play-icon.png" alt=""  data-no-retina="">
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<a href="#about" class="m-t100 d-block page-scroll">
			<div class="mouse-wrapper">
				<div class="mouse">
				</div>
			</div>
		</a>
	</div><!--/.container-->
</header>
<!--/BANNER-->
<!--BANNER-->
<header class="top_banner mobile-version" id="home">
    <div class="container_custom">
        <div class="row h-100">
            <div class="col-md-6 align-self-center wow animate fadeIn">
                <div class="about_left_txt">
                    <div class="title  title-header-cus-width">
                        <p class="title-banner-trackie">Be There For Your Children, Even When You're Apart</p>

                    </div><!--/.title-->

                    <div class="sub_txt  title-header-cus-width-description">
                        <p class="title-banner-trackie-description">With the TrackieX GPS tracker and mobile app, you’ll be able to know where your children are in real-time so you can have peace of mind wherever they go.
                        </p>


                    </div>

                    <div id="menu_bars" class="left menu_bars_head_btn">
                        <div class="btn-group" id="extra_nav">
                            <a href="https://trackiex.com/Trackiex-blog/product/trackiex-kids-2g/" class="btn">Pre-Order Now</a>
                        </div>
                    </div>
                </div><!--/.about_left_txt-->
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 align-self-end display-table mob-margin sm-margin-50px-bottom xs-margin-30px-bottom wow animate fadeIn" >
                <div class="display-table-cell vertical-align-middle">
                    <a class="popup-youtube" href="https://www.youtube.com/watch?v=smZsZ7xafQQ">
                        <img src="img/header-mob-phone.png"  alt="" class="width-100 img-responsive" data-no-retina="">
                        <div class="icon-play">
                            <div class="absolute-middle-center">
                                <img src="img/play-icon.png" alt=""  data-no-retina="">
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <a href="#about" class="m-t100 d-block page-scroll">
            <div class="mouse-wrapper">
                <div class="mouse">
                </div>
            </div>
        </a>
    </div><!--/.container-->
</header>
<!--/BANNER-->

<!--ABOUT US-->
<section class="about_us desktop-version" id="about">
	<div class="container_custom">
		<div class="row">
			<div class="col-md-6 wow animate fadeInLeft">
				<div class="about_right_img about_trackiex_app_img">
					<img src="img/trackiex-iphoneapp.png" class="img-responsive" width="575" height="719" alt=""/>
				</div><!--/.about_right_img-->
			</div>
			<div class="col-md-6 wow animate fadeInRight">
				<div class="about_left_txt">
					<div class="title">
						<p class="section-about-title">Just Because You’re Busy Doesn’t Mean You Don’t Care</p>
					</div><!--/.title-->
					<div class="about_txt">
						<p>As busy parents we have a lot to keep track of - working our jobs, cleaning the house, cooking dinner, making sure our children do their homework, getting them to cricket practice... The to-do list never ends.</p>

						<p>And with everything you need to do, all the places you and your children need to be, you can’t always keep an eye on them.</p>

						<p>While they’re away, you worry about their safety.</p>
						<p>And when it’s time to pick them up, sometimes they’re not where you’re supposed to meet them - which can be frustrating at best and frightening at worst.</p>

						<p>But just because you’re a busy parent who can’t be with your children all the time, doesn’t mean you can’t be a good parent.</p>
						<p>With the TrackieX GPS tracker and mobile app, you can stay connected and make sure they’re safe - wherever they go.</p>
					</div>
				</div><!--/.about_left_txt-->

			</div>
		</div>
	</div><!--/.container-->
</section>
<!--/ABOUT US-->

<!--ABOUT US-->
<section class="about_us mobile-version" id="about">
	<div class="container_custom">
		<div class="row">
			<div class="col-md-6 wow animate fadeIn">
				<div class="about_right_img about_trackiex_app_img">
					<img src="img/trackiex-iphoneapp.png" class="img-responsive" width="575" height="719" alt=""/>
				</div><!--/.about_right_img-->
			</div>
			<div class="col-md-6 wow animate fadeIn">
				<div class="about_left_txt">
					<div class="title">
						<p class="section-about-title">Just Because You’re Busy Doesn’t Mean You Don’t Care</p>
					</div><!--/.title-->
					<div class="about_txt">
						<p>As busy parents we have a lot to keep track of - working our jobs, cleaning the house, cooking dinner, making sure our children do their homework, getting them to cricket practice... The to-do list never ends.</p>

						<p>And with everything you need to do, all the places you and your children need to be, you can’t always keep an eye on them.</p>

						<p>While they’re away, you worry about their safety.</p>
						<p>And when it’s time to pick them up, sometimes they’re not where you’re supposed to meet them - which can be frustrating at best and frightening at worst.</p>

						<p>But just because you’re a busy parent who can’t be with your children all the time, doesn’t mean you can’t be a good parent.</p>
						<p>With the TrackieX GPS tracker and mobile app, you can stay connected and make sure they’re safe - wherever they go.</p>
					</div>
				</div><!--/.about_left_txt-->

			</div>
		</div>
	</div><!--/.container-->
</section>
<!--/ABOUT US-->

<!--GRID CARDS-->
<section class="r_blog desktop-version" id="blog">
	<div class="container_custom_section">
		<div class="row">

			<div class="w-100"></div>
			<div class="col-md-4 wow fadeInUp">
				<div class="card">
					<img src="img/trackiex-feature-1.png" class="img-responsive" alt="" width="375" height="245" />
					<div class="card-body">
						<h2>Know Where Your Children Are in Real-Time</h2>
						<p>The TrackieX GPS tracker monitors your children anywhere in the world and sends their location to your mobile phone for 24/7 peace of mind.</p>
						<!--<a href="#" class="btn">read more</a>-->
					</div>
				</div><!--/.card-->
			</div>
			<div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
				<div class="card">
					<img src="img/trackiex-feature-2.png" class="img-responsive" alt="" width="375" height="245" />
					<div class="card-body">
						<h2>Make Sure They Get There <br> On Time</h2>
						<p>TrackieX sends you instant alerts when your children get to school, football practice, or a friends house so you know they’ve arrived safe and sound.</p>
						<!--<a href="#" class="btn">read more</a>-->
					</div>
				</div><!--/.card-->
			</div>
			<div class="col-md-4 wow fadeInUp" data-wow-delay="0.4s">
				<div class="card">
					<img src="img/trackiex-feature-3.png" class="img-responsive" alt="" width="375" height="245" />
					<div class="card-body">
						<h2>Be There to Pick Them Up</h2>
						<p>Wondering where your children are at pick up time? Find them fast when they’re not where you expect to find them or get instant alerts when they’re about to arrive home so you can be outside to greet them..</p>
						<!--<a href="#" class="btn">read more</a>-->
					</div>
				</div><!--/.card-->
			</div>
		</div>
	</div><!--/.container-->
</section>
<!--/GRID CARDS-->

<!--GRID CARDS-->
<section class="r_blog mobile-version" id="blog">
	<div class="container_custom_section">
		<div class="row">

			<div class="w-100"></div>
			<div class="col-md-4 wow fadeIn">
				<div class="card">
					<img src="img/trackiex-feature-1.png" class="img-responsive" alt="" width="375" height="245" />
					<div class="card-body">
						<h2>Know Where Your Children Are in Real-Time</h2>
						<p>The TrackieX GPS tracker monitors your children anywhere in the world and sends their location to your mobile phone for 24/7 peace of mind.</p>
						<!--<a href="#" class="btn">read more</a>-->
					</div>
				</div><!--/.card-->
			</div>
			<div class="col-md-4 wow fadeIn">
				<div class="card">
					<img src="img/trackiex-feature-2.png" class="img-responsive" alt="" width="375" height="245" />
					<div class="card-body">
						<h2>Make Sure They Get There <br> On Time</h2>
						<p>TrackieX sends you instant alerts when your children get to school, football practice, or a friends house so you know they’ve arrived safe and sound.</p>
						<!--<a href="#" class="btn">read more</a>-->
					</div>
				</div><!--/.card-->
			</div>
			<div class="col-md-4 wow fadeIn">
				<div class="card">
					<img src="img/trackiex-feature-3.png" class="img-responsive" alt="" width="375" height="245" />
					<div class="card-body">
						<h2>Be There to Pick Them Up</h2>
						<p>Wondering where your children are at pick up time? Find them fast when they’re not where you expect to find them or get instant alerts when they’re about to arrive home so you can be outside to greet them..</p>
						<!--<a href="#" class="btn">read more</a>-->
					</div>
				</div><!--/.card-->
			</div>
		</div>
	</div><!--/.container-->
</section>
<!--/GRID CARDS-->

<!--BEST FEATUERES-->
<section class="b_features desktop-version" id="features">
	<div class="container_feature">
		<div class="row">
			<div class="col-md-6 wow animate fadeIn">
				<div class="about_right_img">
					<img src="img/trackiex-device.png" class="img-responsive" width="517" height="491" alt=""/>
				</div><!--/.about_right_img-->
			</div>
			<div class="col-md-6 align-self-center wow animate fadeIn">
				<div class="feature">
					<div class="title">
						<p class="section-about-title section-mob-title">How TrackieX Keeps You Close
						</p>
					</div><!--/.title-->
					<div class="about_txt">
						<p>Our app is an essential part of TrackieX experience and lets
							you keep your child within your sight. Now also in your desktop.
							You can use app.trackiex.com when sitting at your computer.
							Its never been easier to keep your track within your sight.
						</p>

			</div>
				</div><!--/.about_left_txt-->

			</div>
			<div class="w-100"></div>
			<div class="col-md-4  wow animate fadeInUp" >
				<div class="single_feature">
					<div class="feature_icon">
						<img src="img/feature/icon-1.png" class="show_icon" alt=""/>
						<img src="img/feature/icon-1hv.png" class="hide_icon" alt=""/>
					</div><!--/.single_feature-->
					<div class="feature_txt">
						<h4>Real-Time Tracking</h4>
						<p>The TrackieX GPS tracker sends location data to your mobile phone in real-time, 24/7.
						</p>
					</div>
					<div class="line1"></div>
					<div class="line2"></div>
				</div><!--/.single_feature-->
			</div>
			<div class="col-md-4  wow animate fadeInUp" data-wow-delay="0.2s">
				<div class="single_feature">
					<div class="feature_icon">
						<img src="img/feature/icon-2.png" class="show_icon" alt=""/>
						<img src="img/feature/icon-2hv.png" class="hide_icon" alt=""/>
					</div><!--/.single_feature-->
					<div class="feature_txt">
						<h4>Geofences and Scheduling</h4>
						<p>Use the mobile app to set geofenced safe zones that alert you when your children enter or leave specific areas like school or your neighborhood. Personal schedules monitor these areas at specific times.</p>
					</div>
					<div class="line1"></div>
					<div class="line2"></div>
				</div><!--/.single_feature-->
			</div>
			<div class="col-md-4  wow animate fadeInUp" data-wow-delay="0.4s">
				<div class="single_feature">
					<div class="feature_icon">
						<img src="img/feature/icon-3.png" class="show_icon" alt=""/>
						<img src="img/feature/icon-3hv.png" class="hide_icon" alt=""/>
					</div><!--/.single_feature-->
					<div class="feature_txt">
						<h4>Remote Voice Monitoring</h4>
						<p>One way voice monitoring lets you hear your children’s surroundings to see if they’re safe with the push of a button.</p>
					</div>
					<div class="line1"></div>
					<div class="line2"></div>
				</div><!--/.single_feature-->
			</div>

			<div class="col-md-4  wow animate fadeInUp" >
				<div class="single_feature">
					<div class="feature_icon">
						<img src="img/feature/icon-4.png" class="show_icon" alt=""/>
						<img src="img/feature/icon-4hv.png" class="hide_icon" alt=""/>
					</div><!--/.single_feature-->
					<div class="feature_txt">
						<h4>Route History</h4>
						<p>See where your children have gone right in the mobile app with 60-day route history.</p>
					</div>
					<div class="line1"></div>
					<div class="line2"></div>
				</div><!--/.single_feature-->
			</div>
			<div class="col-md-4  wow animate fadeInUp" data-wow-delay="0.2s">
				<div class="single_feature">
					<div class="feature_icon">
						<img src="img/feature/icon-5.png" class="show_icon" alt=""/>
						<img src="img/icon/feature5hv.png" class="hide_icon" alt=""/>
					</div><!--/.single_feature-->
					<div class="feature_txt">
						<h4>Track Multiple Devices</h4>
						<p>Need to keep a few children close? Follow as many trackers as you need with our mobile app.
						</p>
					</div>
					<div class="line1"></div>
					<div class="line2"></div>
				</div><!--/.single_feature-->
			</div>
			<div class="col-md-4  wow animate fadeInUp" data-wow-delay="0.4s">
				<div class="single_feature">
					<div class="feature_icon">
						<img src="img/feature/icon-6.png" class="show_icon" alt=""/>
						<img src="img/feature/icon-6hv.png" class="hide_icon" alt=""/>
					</div><!--/.single_feature-->
					<div class="feature_txt">
						<h4>SOS Alerts</h4>
						<p>Your children can send you an instant S.O.S. alert when they’re in trouble by pressing the button on their TrackieX GPS tracker.</p>
					</div>
					<div class="line1"></div>
					<div class="line2"></div>
				</div><!--/.single_feature-->
			</div>
		</div>
	</div><!--/.container-->
</section>
<!--/BEST FEATUERES-->

<!--BEST FEATUERES-->
<section class="b_features mobile-version" id="features">
	<div class="container_feature">
		<div class="row">
			<div class="col-md-6 wow animate fadeIn">
				<div class="about_right_img">
					<img src="img/trackiex-device.png" class="img-responsive" width="517" height="491" alt=""/>
				</div><!--/.about_right_img-->
			</div>
			<div class="col-md-6 align-self-center wow animate fadeIn">
				<div class="feature">
					<div class="title">
						<p class="section-about-title section-mob-title">How TrackieX Keeps You Close
						</p>
					</div><!--/.title-->
					<div class="about_txt">
						<p>Our app is an essential part of TrackieX experience and lets
							you keep your child within your sight. Now also in your desktop.
							You can use app.trackiex.com when sitting at your computer.
							Its never been easier to keep your track within your sight.
						</p>

					</div>
				</div><!--/.about_left_txt-->

			</div>
			<div class="w-100"></div>
			<div class="col-md-4  wow animate fadeIn" >
				<div class="single_feature">
					<div class="feature_icon">
						<img src="img/feature/icon-1.png" class="show_icon" alt=""/>
						<img src="img/feature/icon-1hv.png" class="hide_icon" alt=""/>
					</div><!--/.single_feature-->
					<div class="feature_txt">
						<h4>Real-Time Tracking</h4>
						<p>The TrackieX GPS tracker sends location data to your mobile phone in real-time, 24/7.
						</p>
					</div>
					<div class="line1"></div>
					<div class="line2"></div>
				</div><!--/.single_feature-->
			</div>
			<div class="col-md-4  wow animate fadeIn">
				<div class="single_feature">
					<div class="feature_icon">
						<img src="img/feature/icon-2.png" class="show_icon" alt=""/>
						<img src="img/feature/icon-2hv.png" class="hide_icon" alt=""/>
					</div><!--/.single_feature-->
					<div class="feature_txt">
						<h4>Geofences and Scheduling</h4>
						<p>Use the mobile app to set geofenced safe zones that alert you when your children enter or leave specific areas like school or your neighborhood. Personal schedules monitor these areas at specific times.</p>
					</div>
					<div class="line1"></div>
					<div class="line2"></div>
				</div><!--/.single_feature-->
			</div>
			<div class="col-md-4  wow animate fadeIn">
				<div class="single_feature">
					<div class="feature_icon">
						<img src="img/feature/icon-3.png" class="show_icon" alt=""/>
						<img src="img/feature/icon-3hv.png" class="hide_icon" alt=""/>
					</div><!--/.single_feature-->
					<div class="feature_txt">
						<h4>Remote Voice Monitoring</h4>
						<p>One way voice monitoring lets you hear your children’s surroundings to see if they’re safe with the push of a button.</p>
					</div>
					<div class="line1"></div>
					<div class="line2"></div>
				</div><!--/.single_feature-->
			</div>

			<div class="col-md-4  wow animate fadeIn" >
				<div class="single_feature">
					<div class="feature_icon">
						<img src="img/feature/icon-4.png" class="show_icon" alt=""/>
						<img src="img/feature/icon-4hv.png" class="hide_icon" alt=""/>
					</div><!--/.single_feature-->
					<div class="feature_txt">
						<h4>Route History</h4>
						<p>See where your children have gone right in the mobile app with 60-day route history.</p>
					</div>
					<div class="line1"></div>
					<div class="line2"></div>
				</div><!--/.single_feature-->
			</div>
			<div class="col-md-4  wow animate fadeIn">
				<div class="single_feature">
					<div class="feature_icon">
						<img src="img/feature/icon-5.png" class="show_icon" alt=""/>
						<img src="img/icon/feature5hv.png" class="hide_icon" alt=""/>
					</div><!--/.single_feature-->
					<div class="feature_txt">
						<h4>Track Multiple Devices</h4>
						<p>Need to keep a few children close? Follow as many trackers as you need with our mobile app.
						</p>
					</div>
					<div class="line1"></div>
					<div class="line2"></div>
				</div><!--/.single_feature-->
			</div>
			<div class="col-md-4  wow animate fadeIn">
				<div class="single_feature">
					<div class="feature_icon">
						<img src="img/feature/icon-6.png" class="show_icon" alt=""/>
						<img src="img/feature/icon-6hv.png" class="hide_icon" alt=""/>
					</div><!--/.single_feature-->
					<div class="feature_txt">
						<h4>SOS Alerts</h4>
						<p>Your children can send you an instant S.O.S. alert when they’re in trouble by pressing the button on their TrackieX GPS tracker.</p>
					</div>
					<div class="line1"></div>
					<div class="line2"></div>
				</div><!--/.single_feature-->
			</div>
		</div>
	</div><!--/.container-->
</section>
<!--/BEST FEATUERES-->

<!--STEPS-->
<section class="package_table hidden-mobile" id="packages">
    <div class="container">
        <div class="row"><div class="col-md-8 offset-md-2 wow animate fadeIn">
            <div class="title_package">
                <h3>Peace of Mind in</h3>
                <p>3 Easy Steps</p>
            </div><!--/.title-->
        </div></div>
        <div class="w-100"></div>

        <div class="row">
            <div class="col-md-6 wow animate fadeInRight">
                <div class="row h-100">
                    <div class="col-sm-12 align-self-center">
                        <div class="title">
                            <ul class="list-unstyled ui-steps">
                                <li class="media">
                                    <div class="circle-icon mr-4">1</div>
                                    <div class="media-body">
                                        <p>Download the
                                            Free App</p>
                                    </div>
                                </li>
                            </ul>
                        </div><!--/.title-->
                    </div>
                </div>

            </div>
            <div class="col-md-6 wow animate fadeInLeft">
                <div class="package_right_img">
                    <img src="img/steps/download-app.png" class="img-responsive" width="497" height="289" alt=""/>
                </div><!--/.about_right_img-->
            </div>
        </div>
        <div class="service_single">
            <div class="row">
                <div class="col-md-6 wow animate fadeInLeft">
                    <div class="package_center_img">
                        <img src="img/steps/connect-tracker.png" class="img-responsive" width="497" height="289" alt=""/>
                    </div><!--/.about_right_img-->
                </div>
                <div class="col-md-6 wow animate fadeInRight">
                    <div class="row h-100">
                        <div class="col-sm-12 align-self-center">
                            <div class="title">
                                <ul class="list-unstyled ui-steps">
                                    <li class="media">
                                        <div class="circle-icon mr-4">2</div>
                                        <div class="media-body">
                                            <p>Activate Your SIM and Connect Your Tracker</p>
                                        </div>
                                    </li>
                                </ul>
                                <!--<div class="contact_right_single">-->
                                <!--<img src="img/steps/2.png" alt="">-->
                                <!--<p>Activate Your SIM and Connect Your Tracker</p>-->
                                <!--</div></div>&lt;!&ndash;/.title&ndash;&gt;-->
                            </div>
                        </div>
                    </div>
                </div>
            </div></div>
        <div class="service_single">
            <div class="row">
                <div class="col-md-6 wow animate fadeInRight ">
                    <div class="row h-100">
                        <div class="col-sm-12 align-self-center">
                            <div class="title">
                                <ul class="list-unstyled ui-steps">
                                    <li class="media">
                                        <div class="circle-icon mr-4">3</div>
                                        <div class="media-body">
                                            <p>Place the Tracker in Your Child’s Bag or Pocket</p>
                                        </div>
                                    </li>
                                </ul>
                                <!--<div class="contact_right_single">-->
                                <!--<img src="img/steps/3.png" alt="">-->
                                <!--<p>Place the Tracker in Your Child’s Bag or Pocket</p>-->
                                <!--</div></div>&lt;!&ndash;/.title&ndash;&gt;-->
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 wow animate fadeInLeft">
                    <div class="package_right_img">
                        <img src="img/steps/bag-tracker.png" class="img-responsive" width="497" height="289" alt=""/>
                    </div><!--/.about_right_img-->
                </div>
            </div></div>
    </div>
    </div><!--/.container-->
</section>
<!--/STEPS-->

<!--STEPS MOBILE-->
<section class="package_table hidden-desktop" id="packages">
	<div class="container">
		<div class="row"><div class="col-md-8 offset-md-2 wow animate fadeIn">
			<div class="title_package">
				<h3>Peace of Mind in</h3>
				<p>3 Easy Steps</p>
			</div><!--/.title-->
		</div></div>
			<div class="w-100"></div>
        <div class="service_single">
        <div class="row">
            <div class="col-md-6 wow animate fadeIn ">
                <div class="row h-100">
                    <div class="col-sm-12 align-self-center">
                        <div class="title">

                            <div class="contact_right_single hidden-desktop">
                                <img class="mob-res-img" src="img/steps/mob-1.png" alt="">
                                <p>Download the Free App</p>
                            </div></div><!--/.title-->
                    </div><!--/.title-->
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-6 wow animate fadeIn mob-res-img-2">
        <div class="package_right_img">
            <img src="img/steps/mob-download-1.png" class="img-responsive" width="633" height="366" alt=""/>
        </div><!--/.about_right_img-->
    </div>

		<div class="service_single">
		<div class="row">
			<div class="col-md-6 wow animate fadeIn ">
				<div class="row h-100">
					<div class="col-sm-12 align-self-center">
						<div class="title">

                            <div class="contact_right_single hidden-desktop">
                                <img class="mob-res-img" src="img/steps/mob-2.png" alt="">
                                <p>Activate Your SIM and Connect Your Tracker</p>
                            </div></div><!--/.title-->
                    </div><!--/.title-->
						</div>
					</div>
				</div>

			</div>
			<div class="col-md-6 wow animate fadeIn mob-res-img-2">
				<div class="package_right_img">
					<img src="img/steps/mob-connect-1.png" class="img-responsive" width="633" height="366" alt=""/>
				</div><!--/.about_right_img-->
			</div>

    <div class="service_single">
        <div class="row">
            <div class="col-md-6 wow animate fadeIn ">
                <div class="row h-100">
                    <div class="col-sm-12 align-self-center">
                        <div class="title">

                            <div class="contact_right_single hidden-desktop">
                                <img class="mob-res-img" src="img/steps/mob-3.png" alt="">
                                <p>Place the Tracker in Your Child’s Bag or Pocket</p>
                            </div></div><!--/.title-->
                    </div><!--/.title-->
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-6 wow animate fadeIn mob-res-img-2">
        <div class="package_right_img">
            <img src="img/steps/mob-tracker-1.png" class="img-responsive" width="633" height="366" alt=""/>
        </div><!--/.about_right_img-->
    </div>
		</div></div>
		</div>
	</div><!--/.container-->
</section>
<!--/STEPS MOBILE-->

<!--STORY-->
<section class="our_story" id="story">
	<div class="container_custom">
        <div class="row hidden-desktop">
            <div class="col-md-6 wow animate fadeIn">
                <div class="story_right_img ">
                    <img src="img/our-story-img.png" class="img-responsive our-story-mob-section" width="644" height="558" alt=""/>
                </div><!--/.about_right_img-->

            </div>
            <div class="col-md-6 align-self-center wow animate fadeIn">
                <div class="about_left_txt">
                    <div class="title">
                        <p class="section-story-title">Our Story
                        </p>
                    </div><!--/.title-->
                    <div class="about_txt">
                        <p>As a parent of two young children, I know how hard it can be to balance a career, home duties, and taking care of my children. Even with the help of trusted friends, family, and caretakers, I find myself constantly thinking about my kids - did they get to school on time? Did they get to grandma’s safely? When will they be home?</p>

                        <p>As much as I care about my children, I’ve found worrying about them is quite distracting to my work. And I can cause quite the headache for my mother-in-law when she has to answer my check-in calls every hour, on top of taking care of the kids.</p>

                        <p>While discussing these challenges with a few colleagues, I found out I wasn’t the only one who struggled to balance making sure my children are safe and where they need to be with all my other responsibilities. That’s when I started to do some research to find a solution. </p>
                        <p>And when it’s time to pick them up, sometimes they’re not where you’re supposed to meet them - which can be frustrating at best and frightening at worst.</p>

                        <p>While discussing these challenges with a few colleagues, I found out I wasn’t the only one who struggled to balance making sure my children are safe and where they need to be with all my other responsibilities. That’s when I started to do some research to find a solution. </p>
                    </div>
                    <div class="trackiex-signature">
                        <img src="img/signature.png">
                        <p>Sritharan Nadesh</p>
                        <p>Founder and Director, TrackieX</p>
                    </div>
                </div><!--/.about_left_txt-->
            </div>
        </div>
		<div class="row hidden-mobile">
			<div class="col-md-6 align-self-center wow animate fadeIn">
				<div class="about_left_txt">
					<div class="title">
						<p class="section-story-title">Our Story
						</p>
					</div><!--/.title-->
					<div class="about_txt">
						<p>As a parent of two young children, I know how hard it can be to balance a career, home duties, and taking care of my children. Even with the help of trusted friends, family, and caretakers, I find myself constantly thinking about my kids - did they get to school on time? Did they get to grandma’s safely? When will they be home?</p>

						<p>As much as I care about my children, I’ve found worrying about them is quite distracting to my work. And I can cause quite the headache for my mother-in-law when she has to answer my check-in calls every hour, on top of taking care of the kids.</p>

						<p>While discussing these challenges with a few colleagues, I found out I wasn’t the only one who struggled to balance making sure my children are safe and where they need to be with all my other responsibilities. That’s when I started to do some research to find a solution. </p>
						<p>And when it’s time to pick them up, sometimes they’re not where you’re supposed to meet them - which can be frustrating at best and frightening at worst.</p>

						<p>While discussing these challenges with a few colleagues, I found out I wasn’t the only one who struggled to balance making sure my children are safe and where they need to be with all my other responsibilities. That’s when I started to do some research to find a solution. </p>
					</div>
						<div class="trackiex-signature">
							<img src="img/signature.png">
						<p>Sritharan Nadesh</p>
							<p>Founder and Director, TrackieX</p>

					</div>
				</div><!--/.about_left_txt-->

			</div>
			<div class="col-md-6 wow animate fadeIn">
				<div class="story_right_img ">
					<img src="img/our-story-img.png" class="img-responsive" width="644" height="558" alt=""/>
				</div><!--/.about_right_img-->

			</div>
		</div>
	</div><!--/.container-->
</section>
<!--/STORY-->

<!--GIVING BACK-->
<section class="giving_back hidden-mobile" id="giving_back">
	<div class="container_custom">
		<div class="row">
			<div class="col-md-6 ">
				<!--<div class="wow align-self-end fadeInUpBig sos-web-section">-->
					<!--<img src="img/mob-sos.png" class="img-responsive" width="240" height="80">-->
				<!--</div>-->
			</div>
			<div class="col-md-6 wow fadeInRight">
				<div class="about_left_txt">
					<div class="title">
						<p class="section-givingback-title">Dedicated to Giving Back
						</p>
					</div><!--/.title-->
					<div class="givingback">
						<p>If you’re considering buying a TrackieX GPS tracker, your children are lucky to have a loving, caring parent.</p>

						<p>Unfortunately, not every child has parents to care for them.
							We believe it’s our duty to help those children live the best life they can, which is why we donate Rs. 100 of every TrackieX purchase to SOS Children’s Villages.</p>

						<p>SOS Children’s Villages is a Sri Lankan organization that provides alternative care for children who have lost their parents to ensure they grow up in a stable, loving environment so they can become healthy, independent adults.</p>
					

						<p>
							Learn More about SOS Children’s important work here.</p>
								<p>
							<a href="https://www.soschildrensvillages.lk/" target="_blank">https://www.soschildrensvillages.lk/</a></p>
					</div>
				</div><!--/.about_left_txt-->
			</div>
		</div>
	</div><!--/.container-->
</section>
<!--/GIVING BACK-->

<!--GIVING BACK MOBILE-->
<section class="giving_back hidden-desktop" id="giving_back">
    <div class="container_custom">
        <div class="row">
            <div class="col-md-6 mob-no-padding wow fadeIn">
                <img src="img/giving-mob-back.png" class="img-responsive" width="750" height="604" alt=""/>
            </div>
            <div class="col-md-6 wow fadeIn">
                <div class="about_left_txt">
                    <div class="title">
                        <p class="section-givingback-title">Dedicated to Giving Back
                        </p>
                    </div><!--/.title-->
                    <div class="givingback">
                        <p>If you’re considering buying a TrackieX GPS tracker, your children are lucky to have a loving, caring parent.</p>

                        <p>Unfortunately, not every child has parents to care for them.
                            We believe it’s our duty to help those children live the best life they can, which is why we donate Rs. 100 of every TrackieX purchase to SOS Children’s Villages.</p>

                        <p>SOS Children’s Villages is a Sri Lankan organization that provides alternative care for children who have lost their parents to ensure they grow up in a stable, loving environment so they can become healthy, independent adults.</p>
                        

                        <p>
                            Learn More about SOS Children’s important work here.</p>
                            	<p>
                            <a href="https://www.soschildrensvillages.lk/" target="_blank">https://www.soschildrensvillages.lk/</a></p>
                    </div>
                </div><!--/.about_left_txt-->

            </div>
            <div class="col-md-12 wow fadeIn sos-mobile-section">
                <img src="img/mob-sos.png" class="img-responsive" width="240" height="80">
                </div>
        </div>
    </div><!--/.container-->
</section>
<!--/GIVING BACK MOBILE-->


<!-- TESTIMONIAL -->
<!--<section class="bg-white testimonial wow fadeIn">-->
<!--	<div class="container_custom">-->
<!--		<div class="row"><div class="col-md-8 offset-md-2 wow animate fadeIn">-->
<!--			<div class="title_package">-->
<!--				<h3>What Parents are Saying</h3>-->
<!--			</div><!--/.title-->
<!--		</div></div>-->
<!--		<div class="row position-relative margin-100px-top sm-margin-70px-top xs-margin-50px-top">-->
<!--			<div class="swiper-container swiper-pagination-bottom black-move blog-slider swiper-three-slides">-->
<!--				<div class="swiper-wrapper">-->
					<!-- start testimonial item -->
<!--					<div class="col-md-4 col-sm-6 col-xs-12 swiper-slide sm-margin-four-bottom wow fadeInRight">-->
<!--						<img src="img/avatart-1.png" class="border-radius-100 width-25 margin-25px-bottom sm-margin-15px-bottom" alt=""/>-->

<!--						<div class="margin-half-all bg-testimonial-blue box-shadow-light text-center padding-fourteen-all xs-padding-30px-all">-->
<!--							<p class="testimonial-trackix-title">Solved My Problem</p>-->
<!--							<p class="testimonial-quotes sm-margin-15px-bottom xs-margin-20px-bottom">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>-->
<!--							<span class="text-light-gray2 text-extra-small text-uppercase text-medium-gray">Anoma R</span>-->
<!--						</div>-->
<!--					</div>-->
					<!-- end testimonial item -->
					<!-- start testimonial item -->
<!--					<div class="col-md-4 col-sm-6 col-xs-12 swiper-slide sm-margin-four-bottom wow fadeInRight" data-wow-delay="0.2s">-->
<!--						<img src="img/avatart-2.png" class="border-radius-100 width-25 margin-25px-bottom sm-margin-15px-bottom" alt=""/>-->

<!--						<div class="margin-half-all bg-testimonial-blue box-shadow-light text-center padding-fourteen-all xs-padding-30px-all">-->
<!--							<p class="testimonial-trackix-title">Trackix helped me</p>-->
<!--							<p class="testimonial-quotes sm-margin-15px-bottom xs-margin-20px-bottom">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>-->
<!--							<span class="text-light-gray2 text-extra-small text-uppercase text-medium-gray">Upeksha V</span>-->
<!--						</div>-->
<!--					</div>-->
					<!-- end testimonial item -->
					<!-- start testimonial item -->
<!--					<div class="col-md-4 col-sm-6 col-xs-12 swiper-slide sm-margin-four-bottom wow fadeInRight" data-wow-delay="0.4s">-->
<!--						<img src="img/avatart-3.png" class="border-radius-100 width-25 margin-25px-bottom sm-margin-15px-bottom" alt=""/>-->

<!--						<div class="margin-half-all bg-testimonial-blue box-shadow-light text-center padding-fourteen-all xs-padding-30px-all">-->
<!--							<p class="testimonial-trackix-title">best tracking system</p>-->
<!--							<p class="testimonial-quotes sm-margin-15px-bottom xs-margin-20px-bottom">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>-->
<!--							<span class="text-light-gray2 text-extra-small text-uppercase text-medium-gray">Kamal P</span>-->
<!--						</div>-->
<!--					</div>-->
					<!-- end testimonial item -->
					<!-- start testimonial item -->
<!--					<div class="col-md-4 col-sm-6 col-xs-12 swiper-slide sm-margin-four-bottom wow fadeInRight" data-wow-delay="0.6s">-->
<!--						<img src="img/avatart-1.png" class="border-radius-100 width-25 margin-25px-bottom sm-margin-15px-bottom" alt=""/>-->

<!--						<div class="margin-half-all bg-testimonial-blue box-shadow-light text-center padding-fourteen-all xs-padding-30px-all">-->
<!--							<p class="testimonial-trackix-title">Trackix tracking system</p>-->
<!--							<p class="testimonial-quotes sm-margin-15px-bottom xs-margin-20px-bottom">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>-->
<!--							<span class="text-light-gray2 text-extra-small text-uppercase text-medium-gray">Colson K</span>-->
<!--						</div>-->
<!--					</div>-->

					<!-- end testimonial item -->
<!--				</div>-->
<!--				<div class="swiper-pagination swiper-pagination-three-slides height-auto"></div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--</section>-->
<!-- TESTIMONIAL -->

<!-- ACCORDION -->
<section class="bg-light-gray accordian_section border-none no-padding wow fadeIn">
	<div class="container">
		<div class="row"><div class="col-md-8 offset-md-2 wow fadeIn">
			<div class="title_package">
				<h3>Frequently Asked Questions</h3>
			</div><!--/.title-->
		</div></div>
		<!-- Accordion -->

					<div class="row equalize sm-equalize-auto">
						<div class="col-md-12 col-sm-12 col-xs-12 display-table wow fadeInUp">
							<div class="display-table-cell-vertical-middle padding-thirteen-all md-padding-ten-all sm-padding-six-all xs-padding-50px-tb xs-no-padding-lr">
							<div class="accordion panel-group accordion-style2" id="accordionExample">
								<div class="panel panel-default">
									<div class="panel-heading" id="headingOne">
										<a class="accordion-toggle" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
											<div class="panel-title">
												<span class="question-titles text-extra-dark-gray xs-width-80 display-inline-block">What is TrackieX and who is it for?</span>
												<i class="fas fa-angle-up pull-right text-extra-dark-gray "></i>
											</div>
										</a>
									</div>
								<div id="collapseOne" class="panel-collapse collapse show fade" aria-labelledby="headingOne" data-parent="#accordionExample">
									<div class="panel-body"><p class="accordion_descriptions">TrackieX is a simple to use GPS tracker and mobile app designed to help parents keep track of their children.</p>

										<p class="accordion_descriptions">By attaching our GPS device to your child’s bag, pouch, or belt loop, you can check your child’s location and know they’re safe wherever they are.</p>

										<p class="accordion_descriptions">While TrackieX cannot replace the in-person security you provide as a parent, it can be there for you and your children when you’re physically apart.</p>
									</div>
								</div>
								</div>


								<div class="panel panel-default">
									<div class="panel-heading" id="headingTwo">
										<a class="accordion-toggle" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
											<div class="panel-title">
												<span class="question-titles text-extra-dark-gray xs-width-80 display-inline-block">How does TrackieX work?</span>
												<i class="indicator fas fa-angle-down pull-right text-extra-dark-gray "></i>
											</div>
										</a>



									</div>
									<div id="collapseTwo" class="panel-collapse collapse fade" aria-labelledby="headingTwo" data-parent="#accordionExample">
										<div class="panel-body"><p class="accordion_descriptions">TrackieX uses the combination of a GPS tracker and mobile application to allow you to know your child’s location wherever they go.</p>

											<p class="accordion_descriptions">The highly-accurate GPS tracker sends location data directly and securely to the connected mobile app (which can be installed on multiple phones so both parents and trusted friends and family can check on your child).</p>

											<p class="accordion_descriptions">Within the app, you can configure Geofences, voice monitoring, and more to get alerts on your child’s whereabouts and get the peace of mind that comes from knowing they’re safe.</p>
										</div></div>

								</div>

								<div class="panel panel-default">
									<div class="panel-heading" id="headingThree">
										<a class="accordion-toggle" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
											<div class="panel-title">
												<span class="question-titles text-extra-dark-gray xs-width-80 display-inline-block">How does TrackieX track my child’s location and how accurate is it?</span>
												<i class="indicator fas fa-angle-down pull-right text-extra-dark-gray "></i>
											</div>
										</a>

									</div>
									<div id="collapseThree" class="panel-collapse collapse fade" aria-labelledby="headingThree" data-parent="#accordionExample">
										<div class="panel-body"><p class="accordion_descriptions">By default, the mobile app shows your child’s location using GPS technology included in every TrackieX tracker. It’s accuracy ranges between 5 and 15m depending on signal strength.</p>

											<p class="accordion_descriptions">When GPS location data is not available - for example in some indoor spaces, parking garages, etc, TrackieX uses the included SIM card to connect to GSM networks and determine your child’s location. These networks are more accurate in urban areas, though in general GSM tracking is less accurate than GPS (up to a few hundred meters).</p>

										</div></div>

								</div>
								<div class="panel panel-default">
									<div class="panel-heading" id="headingFour">
										<a class="accordion-toggle" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
											<div class="panel-title">
												<span class="question-titles text-extra-dark-gray xs-width-80 display-inline-block">How often is my child’s location updated in the app?</span>
												<i class="indicator fas fa-angle-down pull-right text-extra-dark-gray "></i>
											</div>
										</a>

									</div>
									<div id="collapseFour" class="panel-collapse collapse fade" aria-labelledby="headingFour" data-parent="#accordionExample">
										<div class="panel-body"><p class="accordion_descriptions">You can determine how often the TrackieX app connects to your child’s tracker using the app’s “connection interval” settings.</p>

											<p class="accordion_descriptions">With this setting, you can choose how frequently you’d like the TrackieX tracker to send location data to the mobile app - for example, every 2 minutes.</p>

											<p class="accordion_descriptions">Then, you can also choose between “Passive” and “Active” modes.</p>
											<p class="accordion_descriptions">In Passive mode, the tracking device goes into a low power “sleep” state until it detects motion. When your child moves, the device then sends location data to the mobile app if enough time has elapsed based on the connection interval you’ve set.</p>
											<p class="accordion_descriptions">For example:  you set a connection interval of 2 minutes and the TrackieX sends location data when your child arrives at school at 8:00. If your child sits down at their desk and doesn’t move again until 9:00, TrackieX will send you that location data at 9:00 and not before (while your child wasn’t moving).</p>
											<p class="accordion_descriptions">For example:  you set a connection interval of 2 minutes and the TrackieX sends location data when your child arrives at school at 8:00. If your child sits down at their desk and doesn’t move again until 9:00, TrackieX will send you that location data at 9:00 and not before (while your child wasn’t moving).</p>
											<p class="accordion_descriptions">For example: when your child arrives at school at 8:00, again at 8:02, again at 8:04, etc. (assuming a 2-minute connection interval).</p>
											<p class="accordion_descriptions">Active mode provides much more up-to-date location data, however this mode uses a lot more battery power than Passive mode.</p>
										</div></div>

								</div>

								<div class="panel panel-default">
									<div class="panel-heading" id="headingFive">
										<a class="accordion-toggle" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
											<div class="panel-title">
												<span class="question-titles text-extra-dark-gray xs-width-80 display-inline-block">Can anyone track my children with TrackieX?</span>
												<i class="indicator fas fa-angle-down pull-right text-extra-dark-gray "></i>
											</div>
										</a>



									</div>
									<div id="collapseFive" class="panel-collapse collapse fade" aria-labelledby="headingFive" data-parent="#accordionExample">
										<div class="panel-body"><p class="accordion_descriptions">No!</p>

											<p class="accordion_descriptions">When the TrackieX mobile tracker is first connected to our mobile app, it makes that smartphone user the Admin.</p>

											<p class="accordion_descriptions">This Admin user can then share that tracker with other users, such as other parents and caretakers.</p>
											<p class="accordion_descriptions">These “shared” users cannot share the tracker with other users or update important settings like connection interval, SOS numbers, or Voice Monitoring numbers.</p>
											<p class="accordion_descriptions">This way, your child’s location can only be seen and monitored by people you trust.</p>
										</div></div>


								</div>

								<div class="panel panel-default">
									<div class="panel-heading" id="headingSix">
										<a class="accordion-toggle" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
											<div class="panel-title">
												<span class="question-titles text-extra-dark-gray xs-width-80 display-inline-block">How is the TrackieX tracker charged and how long does charging take?</span>
												<i class="indicator fas fa-angle-down pull-right text-extra-dark-gray "></i>
											</div>
										</a>



									</div>
									<div id="collapseSix" class="panel-collapse collapse fade" aria-labelledby="headingSix" data-parent="#accordionExample">
										<div class="panel-body"><p class="accordion_descriptions">The TrackieX mobile tracker uses a micro USB connection and cable to charge from a computer USB port or wall plug. Our Kids Plus tracker includes magnetic pins as well for wireless charging.</p>

											<p class="accordion_descriptions">Charging a tracker from zero battery typically takes around 3 hours.</p>

											<p class="accordion_descriptions">During the charging process, a red light will turn on to indicate the tracker is charging; this light will go off when the battery is full.</p>
											<p class="accordion_descriptions">Note: when you first receive your TrackieX mobile tracker, the initial charging process may take up to 8 hours.</p>
										</div></div>



								</div>
								<div class="panel panel-default">
									<div class="panel-heading" id="headingSeven">
										<a class="accordion-toggle" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
											<div class="panel-title">
												<span class="question-titles text-extra-dark-gray xs-width-80 display-inline-block">How much does TrackieX cost?</span>
												<i class="indicator fas fa-angle-down pull-right text-extra-dark-gray "></i>
											</div>
										</a>



									</div>
									<div id="collapseSeven" class="panel-collapse collapse fade" aria-labelledby="headingSeven" data-parent="#accordionExample">
										<div class="panel-body"><p class="accordion_descriptions">TrackieX sells for 15,000 Rs and includes a free 1,000 Rs worth of mobile credits good for one year of usage.*</p>

											<p class="accordion_descriptions">*Usage rates may vary. 1,000 Rs credit is worth approximately 6 hours of voice monitoring and all other typical data usage for one year.</p>

										</div></div>




								</div>


								<div class="panel panel-default">
									<div class="panel-heading" id="headingEight">
										<a class="accordion-toggle" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
											<div class="panel-title">
												<span class="question-titles text-extra-dark-gray xs-width-80 display-inline-block">Why do I need a SIM card and data plan?</span>
												<i class="indicator fas fa-angle-down pull-right text-extra-dark-gray "></i>
											</div>
										</a>



									</div>
									<div id="collapseEight" class="panel-collapse collapse fade" aria-labelledby="headingEight" data-parent="#accordionExample">
										<div class="panel-body"><p class="accordion_descriptions">While TrackieX is a one time purchase with our company, the GPS tracker relies on mobile data and voice networks to provide location data and remote voice monitoring to the free mobile app.</p>

											<p class="accordion_descriptions">That’s why each tracker comes with a SIM card and requires a mobile data plan.</p>

											<p class="accordion_descriptions">When you first purchase a TrackieX, you’ll receive a SIM card pre-loaded with 1000 Rs, which we’ve estimated should cover normal usage for up to 1 year.*</p>
											<p class="accordion_descriptions">After that, you’ll need to pay standard mobile provider fees.</p>
											<p class="accordion_descriptions">*More info about this can be found here.</p>
										</div></div>





								</div>
							</div>

					</div>

			</div>
		</div>
		<!-- .// Accordion -->

	</div>
</section>
<!-- end accordion section -->

<!-- end section -->
<section class="parallax wow fadeIn" data-stellar-background-ratio="0" style="background-color: #fd4943" >
	<div class="opacity-full bg-extra-dark-gray"></div>
	<div class="container position-relative">
		<div class="row">
			<div class="col-lg-9 col-md-10 col-sm-12 text-center center-col wow fadeIn last-paragraph-no-margin">
				<h4 class="alt-font-text text-white hidden-mobile">Know Your Children are Safe <br>
					Wherever They Go
				</h4>
                <h4 class="alt-font-text text-white hidden-desktop">Know Your Children are Safe Wherever They Go
                </h4>
				<a href="https://trackiex.com/Trackiex-blog/product/trackiex-kids-2g/" class="btn order_now_btn hvr-sweep-to-left btn-white btn-small text-extra-small border-radius-4 margin-45px-top xs-no-margin-top">Pre-Order Now</a>
			</div>
		</div>
	</div>
</section>
<!-- end  section -->

<!-- footer -->
<footer class="footer-padding footer-light wow fadeIn">
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="widget">
						<h6 class="text-uppercase">Learn More</h6>
						<ul>
							<li>
								<a href="index.php#story">About us</a>
							</li>
							<li>
								<a href="Trackiex-blog/how-it-works/">How it Works</a>
							</li>
							<li>
								<a href="Trackiex-blog/faq/">FAQ</a>
							</li>
							<!--<li>
								<a href="#">Getting Started Guide</a>
							</li>-->
							<li>
								<a href="Trackiex-blog/contact-us/">Contact</a>
							</li>
							<!--<li>
								<a href="#">Blog</a>
							</li>-->
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<!-- Footer Navigation -->
					<div class="widget">
						<h6 class="text-uppercase">Support</h6>
					<ul>
							<li>
								<a href="Trackiex-blog/contact-us/">Customer Support</a>
							</li>
							<li>
								<a href="Trackiex-blog/return-and-refund/">Return Policy</a>
							</li>
							<li>
								<a href="Trackiex-blog/trackiex-warranty/">EULA</a>
							</li>

						</ul>
					</div>
					<!-- Footer Navigation -->
				</div>
                <div class="col-lg-3 col-md-6 col-sm-6 hidden-mobile">
                    <!-- Footer Navigation -->
                    <div class="widget">
                        <h6 class="text-uppercase">Download THE app</h6>

                        <div class="sh-contacts-widget-item-img">
                            <img src="img/apple_coming_soon.png">
                            </div>

                        <div class="sh-contacts-widget-item-img">
                            <img src="img/google_coming_soon.png">
                            </div>

                    </div>
                    <!-- Footer Navigation -->
                </div>

				<div class="col-lg-3 col-md-6 col-sm-6 hidden-desktop">
					<!-- Footer Navigation -->
					<div class="widget">
						<h6 class="text-uppercase">Download THE app</h6>

						<div class="sh-mobile-view">
							<img src="img/apple_coming_soon.png" class="img-responsive">
                        </div>
					

                        <div class="sh-mobile-view">
                            <img src="img/google_coming_soon.png" class="img-responsive">
                        </div>
                      



					</div>
					<!-- Footer Navigation -->
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<!-- Footer Recent Post -->
				<div class="widget">
						<h6 class="text-uppercase">Follow us On</h6>
						<div class="sh-contacts-widget-item">
							<a href="https://www.facebook.com/trackiex/" target="_blank"><img src="img/social-icons/fb.png" class="img-responsive">
							Facebook</a></div>

						<div class="sh-contacts-widget-item">
							<a href="https://www.instagram.com/trackiex/" target="_blank"><img src="img/social-icons/insta.png" class="img-responsive">
							Instagram</a></div>

						<div class="sh-contacts-widget-item">
							<a href="https://twitter.com/TrackieX?s=09" target="_blank"><img src="img/social-icons/twitter.png" class="img-responsive">
							Twitter</a></div>


					</div>
					<!-- Footer Recent Post -->
				</div>
			</div>
		</div>
	</div>
	<!-- Footer Copyright -->
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!-- Footer Copyright Text -->
					<p class="copyright-text">Copyright © 2018 TrackieX</p>
                    <!--<?php echo date("Y");?>-->
					<p class="copyright-text">Website Design & Development by
						<a href="http://www.ceffectz.com/">Ceffectz</a></p>
					<!-- Footer Copyright Text -->
				</div>

			</div>
		</div>
	</div>
	<!-- Footer Copyright -->
</footer>
<!-- /footer -->

<a href="#" class="scrollup"><i class="fas fa-arrow-up fa-2x"></i></a>



<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/swiper.min.js"></script>
<script src="js/vendor/popper.min.js"></script>
<script src="js/vendor/plugin.js"></script>
<script src="js/main.js"></script>
<script src="js/smooth-scroll.js"></script>

<script>

	$(document).click(function(){
		$('.navbar-collapse').removeClass('show');


	});

//	$(document).click(function(e){
//
//		console.log(e.id);
//	});

</script>
</body>
</html>