window.addEventListener("load", function () {
    var pk = new Piklor(".color-picker", [

            "#fa412d"
            , "#fd3360"
            , "#861599"
            , "#4957c2"
            , "#45ad4a"
            , "#00bbd6"
            , "#00a6f5"
            , "#0b8bee"
            , "#88c440"
            , "#cddc1f"
            , "#d9bd00"
            , "#ff9702"
            , "#01838f"
            , "#5f7c8c"
            , "#7a5449"
            , "#f75709"
            , "#b71b1c"
            , "#ffd600"
            , "#d50300"
            , "#ae1457"
            , "#f60357"
            , "#d500f9"
            , "#aa00ff"
            , "#4a148c"
            , "#26a69a"
        ], {
            open: ".picker-wrapper .btn"
        })
        , wrapperEl = pk.getElm(".picker-wrapper")

        ;

    pk.colorChosen(function (col) {
        wrapperEl.style.backgroundColor = col;

    });
});
