$("#range_01").ionRangeSlider();

$("#range_02").ionRangeSlider({
    min: 100,
    max: 1000,
    from: 550
});
$("#range_03").ionRangeSlider({
    type: "double",
    grid: true,
    min: 0,
    max: 1000,
    from: 200,
    to: 800,
    prefix: "$"
});
$("#range_04").ionRangeSlider({
    type: "double",
    grid: true,
    min: -1000,
    max: 1000,
    from: -500,
    to: 500
});
$("#range_16").ionRangeSlider({
    type: "double",
    min: 1,
    max: 12,
    from: 1,
    to: 12,
    hide_min_max: true,
    hide_from_to: true,
    grid: true
});
$("#range_18").ionRangeSlider({
    type: "double",
    min: 1,
    max: 12,
    from: 1,
    to: 12,
    prefix: "Weight: ",
    postfix: " million pounds",
    decorate_both: false
});
$("#range_22").ionRangeSlider({
    type: "double",
    min: 1,
    max: 12,
    from: 1,
    to: 12,
    hide_min_max: true,
    hide_from_to: true,
    grid: true
});

$("#range_23").ionRangeSlider({
    type: "double",
    min: 1,
    max: 12,
    from: 1,
    to: 12,
    hide_min_max: true,
    hide_from_to: true,
    grid: true
});
