$(document).ready(function(){
// Simple map
      map = new GMaps({
        el: '#gmaps-simple',
        lat: 34.05,
        lng: -78.72,
        zoom : 5,
          zoomControl: false,
          navigationControl: false,
          streetViewControl: false,
          mapTypeControl: false,
          fullscreenControl: false,
      });
    });

// Marker Map


    $(document).ready(function(){
      map = new GMaps({
        el: '#markermap',
        lat: 34.043333,
        lng: -78.028333

      });
      map.addMarker({
        lat: 34.042,
        lng: -78.028333,
        title: 'Marker with InfoWindow',
        infoWindow: {
          content: '<p>Your Content</p>'
        }
      });
    });


// Overlayer


    $(document).ready(function(){
      map = new GMaps({
        el: '#overlayermap',
          lat: 6.9076834,
          lng: 79.8902526,
          zoom : 17,
      });
      map.drawOverlay({
        lat: map.getCenter().lat(),
        lng: map.getCenter().lng(),
        layer: 'overlayLayer',
        content: '<div class="gmaps-overlay">220, Duplication road, colombo 03 <p class="above-text">21 Feb - 28 Feb 2018  |  21:45PM</p> <div class="gmaps-overlay_arrow above"></div></div>',
        verticalAlign: 'top',
        horizontalAlign: 'center'
      });
    });


// Polygonal

$(document).ready(function(){
    map = new GMaps({
        el: '#polymap',
        lat: 6.9076834,
        lng: 79.8902526,
        zoom : 17,
        zoomControl: false,
        navigationControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        fullscreenControl: false,
        click: function(e){
            console.log(e);
        }
    });

    paths = [
        [
            [
                [6.9076834,79.8902526],
                [6.9076834,79.8902526],


            ],[
            [6.9076834,79.8902526],
            [6.9076834,79.8902526],

        ]
        ],[
            [
                [6.9076834,79.8902526],
                [6.9076834,79.8902526],

            ]
        ]
    ];



    path = [[6.908683,79.891055], [6.909549,79.890010], [6.910763,79.889237], [6.910635,79.891640], [6.910550,79.893099],[6.909634,79.893722],[6.908974,79.894258], [6.908314,79.893614], [6.908683,79.891055]  ];
      map.drawPolygon({
        paths: paths,
        useGeoJSON: true,
        strokeColor: '#1dd082',
        strokeOpacity: 0.6,
        strokeWeight: 6,
          content: '<div class="gmaps-overlay">220, Duplication road, colombo 03<div class="gmaps-overlay_arrow above"></div></div>',
      });

      map.drawPolygon({
        paths: path,
        strokeColor: '#1dd082',
        strokeOpacity: 0.6,
        strokeWeight: 6,
          content: '<div class="gmaps-overlay">220, Duplication road, colombo 03<div class="gmaps-overlay_arrow above"></div></div>',
      });

    });


// Routes

$(document).ready(function(){
      map = new GMaps({
        el: '#routesmap',
        lat: -12.043333,
        lng: -77.028333,
          layer: 'overlayLayer',
          content: '<div class="gmaps-overlay">Lima<div class="gmaps-overlay_arrow above"></div></div>',
          verticalAlign: 'top',
          horizontalAlign: 'center'
      });
      map.drawRoute({
        origin: [-12.044012922866312, -77.02470665341184],
        destination: [-12.090814532191756, -77.02271108990476],
        travelMode: 'driving',
        strokeColor: '#131540',
        strokeOpacity: 0.6,
        strokeWeight: 6
      });

    path = [[-12.044012922866312,-77.02470665341184], [-12.090814532191756,-77.02271108990476], [6.910763,79.889237], [6.910635,79.891640], [6.910550,79.893099],[6.909634,79.893722],[6.908974,79.894258], [6.908314,79.893614], [6.908683,79.891055]  ];
    map.drawPolygon({
        paths: paths,
        useGeoJSON: true,
        strokeColor: '#1dd082',
        strokeOpacity: 0.6,
        strokeWeight: 6,
        content: '<div class="gmaps-overlay">Lima<div class="gmaps-overlay_arrow above"></div></div>',
    });

    map.drawPolygon({
        paths: path,
        strokeColor: '#1dd082',
        strokeOpacity: 0.6,
        strokeWeight: 6,
        content: '<div class="gmaps-overlay">Lima<div class="gmaps-overlay_arrow above"></div></div>',
    });
    });

// Styled Map
$(document).ready(function(){

          var map = new GMaps({
          el: "#styledmap",
          lat: 41.895465,
          lng: 12.482324,
          zoom: 17,
          zoomControl : true,
          zoomControlOpt: {
            style : "SMALL",
            position: "TOP_LEFT"
          },
          panControl : true,
          streetViewControl : false,
          mapTypeControl: false,
          overviewMapControl: false
        });

        var styles = [
            {
              stylers: [
                { hue: "#6164c1" },
                { saturation: 20 }
              ]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [
                    { lightness: 200 },
                    { visibility: "simplified" }
              ]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [
                    { visibility: "off" }
              ]
            }
        ];

        map.addStyle({
            styledMapName:"Styled Map",
            styles: styles,
            mapTypeId: "map_style"
        });

        map.setStyle("map_style");
      });